package com.provenlogic.mode_client.adapter;

import java.util.List;

import com.provenlogic.mode_client.HomeActivity;
import com.provenlogic.mode_client.R;

import android.annotation.SuppressLint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class Drawer_Adapter extends BaseAdapter {
	private List<String> menu;
	private HomeActivity activity;
	private LayoutInflater inflater;

	public Drawer_Adapter(List<String> menu, HomeActivity activity) {
		this.menu = menu;
		this.activity = activity;
		inflater = activity.getLayoutInflater();
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return menu.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

	@SuppressLint({ "ViewHolder", "InflateParams" }) @Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView = inflater.inflate(R.layout.drawer_adapter, null);
		ImageView icon = (ImageView) convertView.findViewById(R.id.menu_icon);
		ImageView back = (ImageView) convertView.findViewById(R.id.back);
		TextView title = (TextView) convertView.findViewById(R.id.menu_title);
		title.setText(""+menu.get(position));
		switch (position) {
		case 0:
			icon.setVisibility(View.GONE);
			title.setVisibility(View.GONE);
			back.setVisibility(View.VISIBLE);
			break;
		case 1:
			icon.setImageResource(R.drawable.home);
			icon.setVisibility(View.VISIBLE);
			title.setVisibility(View.VISIBLE);
			back.setVisibility(View.INVISIBLE);
			break;
		case 2:
			icon.setImageResource(R.drawable.profile);
			icon.setVisibility(View.VISIBLE);
			title.setVisibility(View.VISIBLE);
			back.setVisibility(View.INVISIBLE);
			break;
		case 3:
			icon.setImageResource(R.drawable.schedule);
			icon.setVisibility(View.VISIBLE);
			title.setVisibility(View.VISIBLE);
			back.setVisibility(View.INVISIBLE);
			break;
		case 4:
			icon.setImageResource(R.drawable.history);
			icon.setVisibility(View.VISIBLE);
			title.setVisibility(View.VISIBLE);
			back.setVisibility(View.INVISIBLE);
			break;
		case 5:
			icon.setImageResource(R.drawable.points);
			icon.setVisibility(View.VISIBLE);
			title.setVisibility(View.VISIBLE);
			back.setVisibility(View.INVISIBLE);
			break;
		case 6:
			icon.setImageResource(R.drawable.payments);
			icon.setVisibility(View.VISIBLE);
			title.setVisibility(View.VISIBLE);
			back.setVisibility(View.INVISIBLE);
			break;
		case 7:
			icon.setImageResource(R.drawable.about);
			icon.setVisibility(View.VISIBLE);
			title.setVisibility(View.VISIBLE);
			back.setVisibility(View.INVISIBLE);
			break;
		case 8:
			icon.setImageResource(R.drawable.logout);
			icon.setVisibility(View.VISIBLE);
			title.setVisibility(View.VISIBLE);
			back.setVisibility(View.INVISIBLE);
			break;
		default:
			break;
		}
		return convertView;
	}

}
