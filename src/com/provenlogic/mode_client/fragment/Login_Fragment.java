package com.provenlogic.mode_client.fragment;

import com.provenlogic.mode_client.HomeActivity;
import com.provenlogic.mode_client.LoginActivity;
import com.provenlogic.mode_client.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class Login_Fragment extends Fragment{

	LoginActivity activity;
	public Login_Fragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.login,
				container, false);
		activity = (LoginActivity) getActivity();
		activity.toolbar.setVisibility(View.GONE);
		rootView.findViewById(R.id.email_sign_in_button).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(activity,HomeActivity.class);
				activity.startActivity(i);
			}
		});
		rootView.findViewById(R.id.reg).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				activity.getSupportFragmentManager().beginTransaction()
				.replace(R.id.container, new Register1_Fragment()).commit();
			}
		});
		
		return rootView;
	}
}
