package com.provenlogic.mode_client.fragment;

import com.provenlogic.mode_client.LoginActivity;
import com.provenlogic.mode_client.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;

public class Register3_Fragment extends Fragment {

	private LoginActivity activity;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.register3,
				container, false);
		activity = (LoginActivity) getActivity();
		activity.getSupportActionBar().setTitle("");
		rootView.findViewById(R.id.imageView1).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				activity.getSupportFragmentManager().beginTransaction()
				.replace(R.id.container, new Register4_Fragment()).commit();
			}
		});
		return rootView;
	}                  
}
