package com.provenlogic.mode_client.fragment;

import com.provenlogic.mode_client.HomeActivity;
import com.provenlogic.mode_client.LoginActivity;
import com.provenlogic.mode_client.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;

public class Register4_Fragment extends Fragment{
	private LoginActivity activity;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.register4,
				container, false);
		activity = (LoginActivity) getActivity();
		activity.getSupportActionBar().setTitle("");
		rootView.findViewById(R.id.imageView1).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(activity,HomeActivity.class);
				activity.startActivity(i);
			}
		});
		return rootView;
	}
}
