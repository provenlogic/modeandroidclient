package com.provenlogic.mode_client.fragment;

import com.provenlogic.mode_client.LoginActivity;
import com.provenlogic.mode_client.R;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;


public class Register1_Fragment extends Fragment{
	private LoginActivity activity;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.register1,
				container, false);
		TextView tv = (TextView) rootView.findViewById(R.id.textView4);
		String blueString = getResources().getString(R.string.reg_text);
		tv.setText(Html.fromHtml(blueString));
		activity = (LoginActivity) getActivity();
		activity.getWindow().setBackgroundDrawableResource(R.drawable.reg_bg);
		activity.toolbar.setVisibility(View.VISIBLE);
		activity.getSupportActionBar().setTitle("");
		rootView.findViewById(R.id.confirm).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				activity.getSupportFragmentManager().beginTransaction()
				.replace(R.id.container, new Register2_Fragment()).commit();
			}
		});
		return rootView;
	}
}
