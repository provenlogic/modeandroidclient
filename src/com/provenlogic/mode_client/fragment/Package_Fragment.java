package com.provenlogic.mode_client.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.provenlogic.mode_client.HomeActivity;
import com.provenlogic.mode_client.R;

public class Package_Fragment extends Fragment{

	private HomeActivity activity;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.package_fragment,
				container, false);
		activity = (HomeActivity) getActivity();
		activity.getSupportActionBar().setTitle("");
		activity.settitle("Package");
		return rootView;
	}

}
