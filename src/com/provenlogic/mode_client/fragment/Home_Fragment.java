package com.provenlogic.mode_client.fragment;

import java.util.Calendar;

import com.provenlogic.mode_client.HomeActivity;
import com.provenlogic.mode_client.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.RadialPickerLayout;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;

import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;

public class Home_Fragment extends Fragment implements   TimePickerDialog.OnTimeSetListener,
DatePickerDialog.OnDateSetListener{

	private HomeActivity activity;
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.home_fragment,
				container, false);
		
		activity = (HomeActivity) getActivity();
	    rootView.findViewById(R.id.ride).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
						// TODO Auto-generated method stub
						activity.getSupportFragmentManager().beginTransaction()
						.replace(R.id.container, new PickupDrop_Fragment()).commit();
				
				
//				Calendar now = Calendar.getInstance();
//                TimePickerDialog tpd = TimePickerDialog.newInstance(
//                		Home_Fragment.this,
//                        now.get(Calendar.HOUR_OF_DAY),
//                        now.get(Calendar.MINUTE),
//                        false
//                );
//                
//                tpd.setThemeDark(false);
//                tpd.vibrate(true);
//                tpd.dismissOnPause(false);
//                tpd.enableSeconds(false);                
//                tpd.setOnCancelListener(new DialogInterface.OnCancelListener() {
//                    @Override
//                    public void onCancel(DialogInterface dialogInterface) {
//                        Log.d("TimePicker", "Dialog was cancelled");
//                    }
//                });
//                tpd.show(activity.getFragmentManager(), "");
//				DatePickerDialog dpd = DatePickerDialog.newInstance(
//				  Home_Fragment.this,
//				  now.get(Calendar.YEAR),
//				  now.get(Calendar.MONTH),
//				  now.get(Calendar.DAY_OF_MONTH)
//				);
//				dpd.setThemeDark(false);
//				dpd.vibrate(true);
//				dpd.dismissOnPause(false);  
//				dpd.show(activity.getFragmentManager(), "");
                
			}
		});
	    rootView.findViewById(R.id.package_ride).setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				activity.getSupportFragmentManager().beginTransaction()
				.replace(R.id.container, new Package_Fragment()).commit();
			}
		});
		return rootView;
	}
	@Override
	public void onDateSet(DatePickerDialog view, int year, int monthOfYear,
			int dayOfMonth) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute,
			int second) {
		// TODO Auto-generated method stub
		
	}

}
