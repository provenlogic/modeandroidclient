package com.provenlogic.mode_client.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.provenlogic.mode_client.HomeActivity;
import com.provenlogic.mode_client.R;

public class PickupDrop_Fragment extends Fragment{

	HomeActivity activity;
	public PickupDrop_Fragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.pickupdrop,
				container, false);
		activity = (HomeActivity) getActivity();
		
		return rootView;
	}
}
