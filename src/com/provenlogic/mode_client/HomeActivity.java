package com.provenlogic.mode_client;

import java.util.ArrayList;
import java.util.List;

import com.provenlogic.mode_client.adapter.Drawer_Adapter;
import com.provenlogic.mode_client.fragment.Home_Fragment;
import com.provenlogic.mode_client.fragment.Payment_Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class HomeActivity extends AppCompatActivity implements OnItemClickListener{

	public Toolbar toolbar;
	private DrawerLayout mDrawerLayout;
	private ListView mDrawerList;
	private ActionBarDrawerToggle mDrawerToggle;
	private String mDrawerTitle;
	Drawer_Adapter left_drawer_adapter;
	private TextView title;
	private ImageView back;
	private List<String> menu_item = new ArrayList<String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getWindow().setBackgroundDrawableResource(R.drawable.reg_bg);
		setContentView(R.layout.activity_home);
		toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.toggle_menu);
        
        // enabling action bar app icon and behaving it as toggle button
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
 
        title = (TextView) findViewById(R.id.title);
        back = (ImageView) findViewById(R.id.back);
        
        hidetool();
        
        mDrawerLayout = (DrawerLayout) findViewById(R.id.navigation_drawer);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        
        
        
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                toolbar, //nav menu toggle icon
                R.string.app_name, // nav drawer open - description for accessibility
                R.string.app_name // nav drawer close - description for accessibility
        ){
            public void onDrawerClosed(View view) {
                //getActionBar().setTitle(mTitle);
                // calling onPrepareOptionsMenu() to show action bar icons
                invalidateOptionsMenu();
            }
 
            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mDrawerTitle);
                // calling onPrepareOptionsMenu() to hide action bar icons
            }
        };
        
        mDrawerTitle = "Home";
        
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        menu_item.clear();
        menu_item.add("aaa");
        menu_item.add("Home");
        menu_item.add("Profile");
        menu_item.add("Schedules");
        menu_item.add("History");
        menu_item.add("Total Points");
        menu_item.add("Payment");
        menu_item.add("About");
        menu_item.add("Logout");
        
        left_drawer_adapter = new Drawer_Adapter(menu_item, this);
        mDrawerList.setAdapter(left_drawer_adapter);
        mDrawerList.setOnItemClickListener(this);
        
        displayView(0);
	}
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position,
			long id) {
		// TODO Auto-generated method stub
		mDrawerTitle = menu_item.get(position);
		mDrawerLayout.closeDrawer(Gravity.START);
		displayView(0);
	}
	private void displayView(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        switch (position) {
        case 0:
            fragment = new Home_Fragment();
            break;
//        case 1:
//            fragment = new FindPeopleFragment();
//            break;
//        case 2:
//            fragment = new PhotosFragment();
//            break;
//        case 3:
//            fragment = new CommunityFragment();
//            break;
//        case 4:
//            fragment = new PagesFragment();
//            break;
//        case 5:
//            fragment = new WhatsHotFragment();
//            break;
 
        default:
            break;
        }
 
        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .replace(R.id.container, fragment).commit();
 
            // update selected item and title, then close the drawer
            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            mDrawerLayout.closeDrawer(mDrawerList);
        } else {
            // error in creating fragment
            Log.e("MainActivity", "Error in creating fragment");
        }
	}
	public void settitle(String t){
		back.setVisibility(View.VISIBLE);
		title.setVisibility(View.VISIBLE);
		title.setText(t);
	}
	public void hidetool(){
		back.setVisibility(View.GONE);
		title.setVisibility(View.GONE);
	}
}
